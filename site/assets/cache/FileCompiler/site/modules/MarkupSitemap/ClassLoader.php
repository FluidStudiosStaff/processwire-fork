<?php

/**
 * Add applicable namespaces to the ProcessWire classLoader.
 */
\ProcessWire\wire('classLoader')->addNamespace('Rockett\Sitemap', 'C:/Projects/ProcessWire/site/modules/MarkupSitemap' . '/src/Sitemap');
\ProcessWire\wire('classLoader')->addNamespace('Rockett\Traits', 'C:/Projects/ProcessWire/site/modules/MarkupSitemap' . '/src/Traits');