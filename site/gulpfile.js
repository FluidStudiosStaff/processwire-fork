var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var pump = require('pump');
var gulpIf = require('gulp-if');
var cleanCSS = require('gulp-clean-css');
var watch = require('gulp-watch');
var rename = require('gulp-rename');

gulp.task( 'default', [ 'watch' ] )

gulp.task('concat-scripts', function(){
  return gulp.src(['templates/scripts/*.js', '!templates/scripts/all.min.js'])
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest('templates/scripts/'));
});
gulp.task('minify-js', function (cb) {
  pump([
        gulp.src('templates/scripts/main.js', {base: 'templates/scripts/'}),
        uglify(),
		rename({ suffix: '.min' }),
        gulp.dest('templates/scripts')
    ],
    cb
  );
});
gulp.task('sass', function(){
  return gulp.src('templates/styles/scss/**/*.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('templates/styles'))
});

gulp.task('concat-styles',['sass'], function(){
  return gulp.src(['templates/styles/*.css', '!templates/styles/all.min.css'])
    .pipe(concat('all.min.css'))
    .pipe(gulp.dest('templates/styles/'));
});
gulp.task('minify-css',['sass', 'concat-styles'],function() {
  return gulp.src('templates/styles/all.min.css')
    //.pipe(sourcemaps.init())
    .pipe(cleanCSS())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('templates/styles/'));
});

gulp.task('watch', function(){
	gulp.watch('templates/styles/scss/**/*.scss', ['minify-css']); 
	gulp.watch('templates/scripts/**/*.js', ['minify-js']);
});