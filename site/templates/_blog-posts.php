<?php
//

/**
* Create a feed of latest blog posts with previews
*/

//echo '<div class="row blog-previews" >';

// load blog posts
// set allowed blog templates, order and max number of posts to load here (10)
	$posts =  $pages->find("template=blog-post, sort=-post_date, limit=10, check_access=0");

	// loop through the posts
	foreach ($posts as $p){
		echo '<article>';
		echo '	<a href="' . $p->url . '">';
		$image = $p->blog_img;
		if (!empty($image)) {
			echo '	<div class="blog-img-wrapper">';
			echo '    <img src="' . $image->url . '" alt="' . $p->title . '">';
			echo '  </div>';
		};
		echo '		<h2>' . $p->title . '</h2>';
		echo '    <p>' . substr($p->Blog_TextArea1, 0, 191) . '...</p>';
		echo '	</a>';
		echo '</article>';
  }
?>