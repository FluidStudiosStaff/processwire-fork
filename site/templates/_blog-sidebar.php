<?php
//

/**
* Create a sidebar for the blog showing an archive if links.  It will show the year and month if they contain any sPosts, and a list of blog post titles / links under them.
*/

?>
<ul class="blog-sidebar" id="accordian" aria-multiselectable="true">
<?php

// sCount to check which post is 1st
$sCount = 0;
// after how many open lines should the years collapse
$sCollapseLimit = 10;

// load blog sPosts
// set allowed blog templates, order and max number of sPosts to load here (1000)
$sPosts =  $pages->find("template=blog-post, sort=-post_date, limit=1000");

        foreach ($sPosts as $sp)  // loop through the sPosts
        {
			//grab the post year and month
			$year = date('Y', $sp->blog_date);
            $month = date('m', $sp->blog_date);
			$monthName = date('F', mktime(0, 0, 0, $month, 10));

			// if first blog post, show the year and month
			if ($sCount == 0) {
				echo '<li class="blog-year-link"><h3><a data-toggle="collapse"  data-parent="#accordion" href="#id' . $year . '" aria-expanded="true" aria-controls="id' . $year . '">' . $year . '</a></h3></li><li><ul id="id' . $year . '" class="blog-side-year collapse show">';
				echo "<h6><li class=\"blog-side-month\">" . $monthName . "</li></h6>";

				// store the year month so we can check when they change
				$currentMonth = $month;
				$currentYear = $year;
				$monthName = date("F", mktime(0, 0, 0, $currentMonth, 10));
				$sCount ++;
			} else {

				// check if year has changed
				if($year != $currentYear) {
					if ($sCount < $sCollapseLimit) {
						echo '</ul></li><li class="blog-year-link"><a data-toggle="collapse" data-parent="#accordion" href="#id' . $year . '" aria-expanded="true" aria-controls="id' . $year . '">' . $year . '</a></li><li><ul id="id' . $year . '" class="blog-side-year collapse show">';
					} else {
						echo '</ul><li class="blog-year-link"><a data-toggle="collapse" data-parent="#accordion" href="#id' . $year . '" aria-expanded="true" aria-controls="id' . $year . '">' . $year . '</a></li><li><ul id="id' . $year . '" class="blog-side-year collapse ">';
					}
					// change the current year
					$currentYear = $year;
					$sCount ++;
				}
				// check in month has changed
				if($month != $currentMonth) {
					// display the current month
					$monthName = date("F", mktime(0, 0, 0, $month, 10));
					echo "<h6><li class=\"blog-side-month\">" . $monthName . "</li></h6>";
					// change the current month
					$currentMonth = $month;
					$sCount ++;
				}

			}
			// display sPosts within the current month
			echo '<li class="archive-post"><a href="' . $sp->url . '">' . $sp->title . '</a></li>';
			$sCount ++;
        }
		//close year list
		echo '</ul></li>';

?>
</ul>
