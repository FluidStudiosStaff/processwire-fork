<footer>
	<div class="flex-wrapper footer-btm-wrapper"> 
		<div class="flex-50 footer-btm-left">
			© Copyright 2018. <a href="https://www.fluidstudiosltd.com" title="web design hertfordshire" target="_blank">Web design by Fluid</a>. <a href="https://www.bigreach.co.uk" title="seo hertfordshire" target="_blank">SEO by Big Reach.</a>
		</div>
		<div class="flex-50 footer-btm-right">
			<a href="#" class="top bt-top footer-link">Back to top</a>
		</div>
	</div>
</footer>