<!-- Class that links to 'back to top' link in footer. Check main.js to remove/change scroll speed -->
<div id="top"></div>

<!-- Flex Navigation -->

<nav class="flex-navigation">

	<div class="navigation-toggle">
		<i class="fa fa-bars" aria-hidden="true"></i>
	</div>
	<div class="navigation-items">
		<div class="logo">
            <a href="/">
                <img class="" src="/site/assets/logo-2018.png">
            </a>
        </div>
		<ul class="navigation">
			<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
			<?php echo $navigation; ?>
		</ul>
	</div>
</nav>
