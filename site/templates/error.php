<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<?php include("./_func.php");?>
		<?php include("./_head.php");?>
	</head>
	<body>
		<?php include("./_header.php");?>
		<div id="gen-banner-wrap">
		<?php include("./_slick.php");
			echo $slickBanner;?>
            <div class="banner-text-wrap">
                <div class="gen-banner-text">
                    <?php echo $page->BannerText;?>
                </div>
            </div>
		</div>
		<main>

		<div class="err-wrap-1">
			<div class="max-width">
                    <?php echo $page->error_message;?>
			</div>
		</div>
        	

		</main>

		<?php include("./_footer.php");?>
		<?php include("./_scripts.php");?>
		<?php // for an edit link when loggind in add:   if($page->editable()) echo "<p><a href='$page->editURL'>Edit</a></p>"; ?>
	</body>
</html>