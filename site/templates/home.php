<?php namespace ProcessWire; ?>

<!DOCTYPE html>
<html lang="en-GB">
	<head>
		<?php include("./_func.php");?>
		<?php include("./_head.php");?>
	</head>
	<body>
		<?php include("./_header.php");?>

		<div id="banner-wrap">
		<?php include("./_slick.php");
			echo $slickBanner;?>
            <div class="banner-text-wrap">
                <div class="banner-text">
                    <?php echo $page->BannerText;?>
                </div>
            </div>
		</div>


		<div class="content-container index-content-1"> 
			<div class="max-width">
				<?php echo $page->index_CKEditor1?>
			</div>
		</div>

		</div>
		<?php include("./_footer.php");?>
		<?php include("./_scripts.php");?>
		<?php // for an edit link when loggind in add:   if($page->editable()) echo "<p><a href='$page->editURL'>Edit</a></p>"; ?>
	</body>
</html>

