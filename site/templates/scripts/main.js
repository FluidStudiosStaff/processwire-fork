$(document).ready(function() {
	adjustNav();
});

$(window).resize(function() {
	adjustNav();
});

$('.navigation-toggle').click(function() {
	if ($('.navigation-items').hasClass('nav-showing')) {
		$('.navigation-items').slideUp().removeClass('nav-showing');
	} else {
		$('.navigation-items').slideDown().addClass('nav-showing');
	}
});

function adjustNav() {
	if ($(window).width() <= 979) {
		$('.navigation-toggle').css('display', 'block');
		$('.navigation-items').slideUp();
	} else {
		$('.navigation-toggle').css('display', 'none');
		$('.navigation-items').slideDown();
	}
}

$(function() {
$(".footer-link").click(function() {
    $('html, body').animate({
        scrollTop: $("#top").offset().top
    }, 1000);
});
});
